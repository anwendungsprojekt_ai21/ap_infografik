var database = require("./database.js");

module.exports.updateCycle = function updateCycle() {
  database.getAll().then((data) => {
    setRessource(data);
  });
};

function setRessource(jfile) {
  for (var i = 0; i < jfile.length; i++) {
    //Werte aus JSon bekommen
    var bettenNormalAuslastung =
      jfile[i]["ressourcen"]["betten"]["normalauslastung"];
    var bettenIntensivAuslastung =
      jfile[i]["ressourcen"]["betten"]["intensivauslastung"];
    var kittel = jfile[i]["ressourcen"]["schutzkleidung"]["kittel"];
    var handschuhe = jfile[i]["ressourcen"]["schutzkleidung"]["handschuhe"];
    var masken = jfile[i]["ressourcen"]["schutzkleidung"]["masken"];
    var desinfektionsmittel =
      jfile[i]["ressourcen"]["materialien"]["desinfektionsmittel"];
    var beatmungsgeraete =
      jfile[i]["ressourcen"]["materialien"]["beatmungsgeraete"];
    var ressourcenMaxi = 1000000;
    var atemgeraeteMaxi = 1000000;
    var desinfektionsmittelMaxi = 30000000000;

    //neue Werte setzen
    jfile[i]["ressourcen"]["betten"]["normalauslastung"] = calculateBetten(
      bettenNormalAuslastung
    );
    jfile[i]["ressourcen"]["betten"]["intensivauslastung"] = calculateBetten(
      bettenIntensivAuslastung
    );
    jfile[i]["ressourcen"]["schutzkleidung"]["kittel"] = calculateRessourcen(
      ressourcenMaxi,
      kittel
    );
    jfile[i]["ressourcen"]["schutzkleidung"][
      "handschuhe"
    ] = calculateRessourcen(ressourcenMaxi, handschuhe);
    jfile[i]["ressourcen"]["schutzkleidung"]["masken"] = calculateRessourcen(
      ressourcenMaxi,
      masken
    );
    jfile[i]["ressourcen"]["materialien"][
      "desinfektionsmittel"
    ] = calculateRessourcen(desinfektionsmittelMaxi, desinfektionsmittel);
    jfile[i]["ressourcen"]["materialien"][
      "beatmungsgeraete"
    ] = calculateRessourcen(atemgeraeteMaxi, beatmungsgeraete);

    database.updateOne(jfile[i]['_id'], jfile[i]);
  }
}

function calculateRessourcen(max, ressource) {
  if (max != ressource && ressource != 0) {
    var neu = Math.ceil(ressource * ((Math.random() / 10) + 0.95));
  } else if (max != ressource && ressource == 0) {
    var neu = 1;
  } else {
    var neu = Math.ceil(ressource * (Math.random() /20 + 0.95));
  }
  if (neu > max) {
    neu = max;
  }
  return neu;
}
/**
 * Berechnet neuen Auslastungswert, der +-10% vom alten Wert abweicht.
 * @param {*} auslastung alter Auslastungswert
 */
function calculateBetten(auslastung) {
  auslastung =
    auslastung + (Math.random()/5 -0.1);

  //Kontrolliere Wert auf Korrektheit
  if (auslastung <= 1 && auslastung >= 0) {
    return auslastung;
  } else {
    if (auslastung > 1) {
      return 1;
    } else {
      return 0;
    }
  }
}
