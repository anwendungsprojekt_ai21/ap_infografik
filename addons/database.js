var mongoose = require('mongoose');

KrankenhausObject = {
    "name": String,
    "adresse": {
      "strasse": String,
      "postleitzahl": Number,
      "stadt": String
    },
    "koordinaten":{
        "lat": Number,
        "lon": Number
    },
    "besonderes": String,
    "spezialisierung": String,

    "ressourcen":{
        "betten": {
				    "normal": Number,
				    "normalauslastung": Number,
				    "intensiv": Number,
				    "intensivauslastung": Number
			},
        "schutzkleidung":{
            "kittel": Number,
            "handschuhe": Number,
            "masken": Number
        },
        "personal":{
            "personalleitung": String,
            "aerzte": Number,
            "krankenpfleger": Number
        },
        "materialien":{
            "desinfektionsmittel": Number,
            "beatmungsgeraete": Number
        }
    }
}

var KrankenhausSchema = new mongoose.Schema(KrankenhausObject);

const KrankenhausModel = mongoose.model('KrankenhausSammlung', KrankenhausSchema);

function getDocuments(model) {
    return model.find().then((result) => { return result })
                                          .catch((err) => { console.log(err); });
};

module.exports.getAll = function getAll() {
  return getDocuments(KrankenhausModel).then((data) => {
      return data;
  });
}

module.exports.updateOne = function updateOne(id, object) {
    KrankenhausModel.updateOne({_id: id}, object, function(err, resp) {
        if (err) {
            console.log("Not bien");
        }
    })
}

module.exports.insertKrankenhaus = function insertKrankenhaus(newobject) {
    var aKeys = Object.keys(newobject).sort();
    var bKeys = Object.keys(KrankenhausObject).sort();
    if (JSON.stringify(aKeys) === JSON.stringify(bKeys)) {
    newDocument = KrankenhausModel(newobject);
    newDocument.save();
    return true;
    }
    return false;
}
