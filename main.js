var mongoose = require("mongoose");
const express = require("express");
var database = require("./addons/database.js");
var simulation = require("./addons/simulation.js");
const app = new express();
const bodyParser = require("body-parser");

const key = "";
const PORT = 8080;
const HOST = "0.0.0.0";
const HOST_MONGODB = "172.17.0.1";

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", function (request, response) {
  response.sendFile(__dirname + "/html/uebersicht/uebersicht.html");
});

app.get("/api/krankenhausdata.json", function (request, response) {
  database.getAll().then((data) => {
    response.send(data);
  });
});

app.get("/uebersicht", function (request, response) {
  response.sendFile(__dirname + "/html/uebersicht/uebersicht.html");
});

app.get("/uebersicht.css", function (request, response) {
  response.sendFile(__dirname + "/html/uebersicht/uebersicht.css");
});

app.get("/uebersicht.js", function (request, response) {
  response.sendFile(__dirname + "/html/uebersicht/uebersicht.js");
});

app.get("/impressum", function (request, response) {
  response.sendFile(__dirname + "/html/impressum/impressum.html");
});

app.get("/impressum.css", function (request, response) {
  response.sendFile(__dirname + "/html/impressum/impressum.css");
});

app.get("/navigationsleiste.css", function (request, response) {
  response.sendFile(
    __dirname + "/html/navigationsleisteTemplate/navigationsleiste.css"
  );
});

app.get("/list_filter.js", function (request, response) {
  response.sendFile(__dirname + "/html/scripts/list_filter.js");
});

app.post("/api/newdata.json", function (request, response) {
  responseCode = 200;
  request.body.forEach((item) => {
    let res = database.insertKrankenhaus(item);
    responseCode = res ? responseCode : 422;
  });
  response.sendStatus(responseCode);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

app.get("/karte", function (request, response) {
  response.sendFile(
    __dirname + "/html/Kartenübersicht/kartenuebersicht.html"
  );
});

app.get("/Kartenuebersicht.css", function (request, response) {
  response.sendFile(__dirname + "/html/Kartenübersicht/kartenuebersicht.css");
});

app.get("/kartenuebersicht.js", function (request, response) {
  response.sendFile(__dirname + "/html/Kartenübersicht/kartenuebersicht.js");
});

app.get("/Krankenhaus.jpg", function (request, response) {
  response.sendFile(__dirname + "/html/Kartenübersicht/Krankenhaus.jpg");
});

app.get("/middl.jpg", function (request, response) {
  response.sendFile(__dirname + "/html/Kartenübersicht/middl.jpg");
});

app.get("/post", function (request, response) {
  response.sendFile(__dirname + "/html/post/post.html");
});

// Connection to MongoDB
mongoose.connect(
  `mongodb://${key}@${HOST_MONGODB}:27017/ap_infografik?authSource=admin`,
  { useNewUrlParser: true }
);

mongoose.connection.on(
  "error",
  console.error.bind(console, "connection error:")
);
mongoose.connection.once("open", function () {
  console.log("Mongoose successfully connected");
});

setInterval(simulation.updateCycle, 1000 * 60 * 2);
