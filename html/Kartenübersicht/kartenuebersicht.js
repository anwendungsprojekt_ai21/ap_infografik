export function setMap(jfile) {
    document.getElementById("Karte").innerHTML = "";
    
    var names = [];
    var latKordinateEingang = [];
    var longKordinateEingang = [];
    var latKordinate = [];
    var longKordinate = [];
    var counter = 0;
    var divsKrankenhaeuser = "";

    for (var i = 0; i < jfile.length; i++) {
      names[i] = jfile[i]["name"];
      latKordinateEingang[i] = jfile[i]["koordinaten"]["lat"];
      longKordinateEingang[i] = jfile[i]["koordinaten"]["lon"];
    }

    //Rechnet PX in Verhältnissse zu Bildgröße um(%)
    for (counter = 0; counter < longKordinateEingang.length; counter++) {
      latKordinate[counter] =
        ((72 * latKordinateEingang[counter] + 18 * 72 - 30) / 1791) * 100 +
        "%";
    }

    //Rechnet PX in Verhältnissse zu Bildgröße um(%)

    for (counter = 0; counter < longKordinateEingang.length; counter++) {
      longKordinate[counter] =
        ((72 * (15 - longKordinateEingang[counter]) - 150) / 1706) * 100 +
        "%";
    }

    //Ruft createKrankenhaus so oft aus, wie Krankenhäuser existieren(%)
    for (counter = 0; counter < names.length; counter++) {
      divsKrankenhaeuser =
        divsKrankenhaeuser +
        '<div class= "Krankenhaus" style="margin-left: ' +
        latKordinate[counter] +
        ";margin-top:" +
        longKordinate[counter] +
        ';"> <div class= "krankenhausname">' +
        names[counter] +
        "</div> </div>";
      document.getElementById("Karte").innerHTML = divsKrankenhaeuser;
    }
  }


// async call with a callback function
export function httpGet(theUrl, callback, callback2 = function undef() {}){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", theUrl, true);
    xhr.onload = function (e) {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
          callback(xhr.responseText);
          callback2();
      } else {
        console.error(xhr.statusText);
        }
      }
    };
    xhr.onerror = function (e) {
      console.error(xhr.statusText);
    };
    xhr.send(null); 
}