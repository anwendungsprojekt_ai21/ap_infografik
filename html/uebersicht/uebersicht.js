/*
Nimmt ein Array an Krankenhausdaten an und erstellt ein HTML string.
Nachträglich wird es in die #list hinzugefügt, wobei zuerst #list entleert wird.
Jeder Krankenhauseintrag wird mit der Klasse "input" gekennzeichnet.
*/
export function inputdata(dataToWrite) {
    document.getElementById("list").innerHTML = '';
    dataToWrite.forEach((krankenhaus) => {
      var inline_string = "";
      inline_string += '<div class="input">';
      // not collapsable information
      inline_string += `<div id="grid-item-name">${krankenhaus.name}</div>`;
      inline_string += `<div id="grid-item-betten">Betten: ${Math.round(krankenhaus.ressourcen.betten.normal * krankenhaus.ressourcen.betten.normalauslastung)}/${krankenhaus.ressourcen.betten.normal}</div>`;
      inline_string += `<div id="grid-item-intensiv">Intensivbetten: ${Math.round(krankenhaus.ressourcen.betten.intensiv * krankenhaus.ressourcen.betten.intensivauslastung)}/${krankenhaus.ressourcen.betten.intensiv}</div>`;
      inline_string += `<div id="grid-item-geraete">Beatmungsgeräte: ${krankenhaus.ressourcen.materialien.beatmungsgeraete}</div>`;   
      //
      inline_string += '<button type="button" class="pure-material-button-contained">+</button>'
      inline_string += '<div class="content">'
      // collapsable information
      inline_string += `<div id="left"> <div class="header">Adresse</div>`;
      inline_string += `<div>${krankenhaus.adresse.strasse}</div>`;
      inline_string += `<div>${krankenhaus.adresse.postleitzahl} ${krankenhaus.adresse.stadt}</div>`;
      inline_string += `<p></p>`;
      inline_string += `<div class="header">Personal</div>`;
      inline_string += `<div>Personalleitung: ${krankenhaus.ressourcen.personal.personalleitung}</div>`;
      inline_string += `<div>Ärzte: ${krankenhaus.ressourcen.personal.aerzte}</div>`;
      inline_string += `<div>Krankenpfleger: ${krankenhaus.ressourcen.personal.krankenpfleger}</div>`;
      inline_string += `</div>`;

      inline_string += `<div id="right">`;
      inline_string += `<div class="header">Spezialisierung</div>\n ${krankenhaus.spezialisierung}`;
      inline_string += `<p></p>`;
      inline_string += `<div class="header">Besonderes</div>\n ${krankenhaus.besonderes}`;
      inline_string += `</div>`;

      inline_string += `<div id="middle">`;
      inline_string += `<div class="header">Ressourcen</div>`;
      inline_string += `<div>Desinfektionsmittel: ${krankenhaus.ressourcen.materialien.desinfektionsmittel} L</div>`;
      inline_string += `<div>Kittel: ${krankenhaus.ressourcen.schutzkleidung.kittel}</div>`;
      inline_string += `<div>Handschuhe: ${krankenhaus.ressourcen.schutzkleidung.handschuhe}</div>`;
      inline_string += `<div>Masken: ${krankenhaus.ressourcen.schutzkleidung.masken}</div>`;
      inline_string += `</div>`;
      //
      inline_string += '</div>';
      inline_string += '</div>';

      document.getElementById("list").innerHTML += inline_string;
    });
  }

  // fügt jedem knopf der klasse "pure-material-button-contained" die Expandierfunktion hinzu
export function addClick() {
    var coll = document.getElementsByClassName("pure-material-button-contained");
    var i;
    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight){
          content.style.maxHeight = null;
          this.innerHTML = "+";
        } else {
          content.style.maxHeight = content.scrollHeight + 30 + "px";
          this.innerHTML = "&ndash;";
        }
      });
    }
  }

// async call with a callback function
export function httpGet(theUrl, callback, callback2 = function undef() {}){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", theUrl, true);
    xhr.onload = function (e) {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
          callback(xhr.responseText);
          callback2();
      } else {
        console.error(xhr.statusText);
        }
      }
    };
    xhr.onerror = function (e) {
      console.error(xhr.statusText);
    };
    xhr.send(null); 
}
