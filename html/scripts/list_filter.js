/**
 * Dieses Script nimmt Liste von Krankenhausobjekten als Array und gibt eine gefilterte/sortierte Liste nach den gegebenen Kriterien zurück.
 *
 */


/**
 * Verwendbare Funktion zum Sortieren und Filtern einer Liste von Krankenhaus Objekten. Gibt formatierte Liste mit unveränderten Objekten zurück.
 * @param {*} list Liste von Krankenhaus Objekten
 * @param {*} sortingCriteria 'name','postleitzahl','spezialisierung','betten','schutzkleidung','personal','materialien','auslastung','default'
 * @param {*} regionCriteria 'eriador','rhovanion','rohan','mordor','nord','süd','ost','west'
 * @param {*} typeCriteria 'krankenhaus','klinik','andere','default'
 */
export function formatList(list, sortingCriteria, regionCriteria, typeCriteria){
    let formattedList = []

    //Zuerst die Liste filtrieren, erst nach region dann nach typus
    formattedList = _filterList(list, regionCriteria)
    formattedList = _filterList(formattedList, typeCriteria)

    //Danach die gefilterte Liste sortieren
    formattedList = _sortList(formattedList, sortingCriteria)
    
    return formattedList
}



 /**
  * Sortiert eine Liste von Krankenhäusern-Objekten nach gegebenem Kriterium.
  * Gibt anschließend die sortierte Liste mit den unveränderten Objekten zurück.
  * @param {*} list Array von Krankenhaus Objekten (Array<Object>)
  * @param {*} sortingCriteria Das Sortierkriterium (string), Möglichkeiten: 'name','postleitzahl','spezialisierung','betten','schutzkleidung','personal','materialien','auslastung'
  */
 function _sortList(list,sortingCriteria){
    //Leere liste, wo sortierte Elemente eingefügt werden.
    let sortedList = []

    //Sortiere die Liste mit Sort Funktion, die eine Vergleichs-Hilfsfunktion benutzt nach jeweiligem Kriterium.
    //sortedList = list.sort((a,b)=> _compare(a,b,sortingCriteria))
    //Konvertiere Buchstaben to lower case
    sortingCriteria = sortingCriteria.toLowerCase()
    console.log(sortingCriteria)
    switch(sortingCriteria){
    
        //Einfache Sortierung nach unverschachtelten Attributen.
        case 'name':
        case 'spezialisierung':
            sortedList = list.sort((a,b) => _simpleCompare(a, b, sortingCriteria))
            break;

        //Kompliziertere Sortierung nach verschachtelten Attributen, speziell die Summe von Gegenständen/Personen.
        case 'betten':
        case 'schutzkleidung':
        case 'personal':
        case 'materialien':
          sortedList = list.sort((a,b) => _sumCompare(a, b, sortingCriteria,'ressourcen'))
          //umdrehen, damit Zahlenwerte aufsteigend sortiert sind
          sortedList = sortedList.reverse()
          break;

        //Postleitzahl, arbeitet mit Basisfunktion _absoluteCompare
        case 'postleitzahl':
          sortedList = list.sort((a,b) => _absoluteCompare(a['adresse']['postleitzahl'], b['adresse']['postleitzahl']))
          break;

        //Spezielle Sortierung nach gewichteter Bettenauslastung.
        case 'auslastung':
          sortedList = list.sort((a,b) => _bedsCompare(a, b))
          break;

        //default gibt Liste unsortiert in ihrem normalen Zustand zurück
        default:
            sortedList = list;
            break;
    }

    //return
    return sortedList
 }


 /**
  * Einfache Hilfsfunktion für die Array.sort() Methode, die bei jeweils zwei Objekten ein Attribut vergleicht und einen Vergleichs Rückgabewert gibt.
  * Es wird das jeweils größere Attribut von einer Zahl/Alphatetischen Reihenfolge als größer gewertet.
  * @param {*} a Ein Objekt
  * @param {*} b Ein anderes Objekt
  * @param {*} sortingCriteria das Sortierkriterium als string, Möglichkeiten: 'name','adresse','spezialisierung'
  */
 function _simpleCompare(a, b, sortingCriteria){
    return _absoluteCompare(a[sortingCriteria],b[sortingCriteria])
 }


 /**
  * Komplexere Hilfsfunktion für Array.sort() speziell für Krankenhaus Objekte, die jeweils die Summe von einer Ressourcenkategorie Vergleicht bei zwei Objekten
  * und einen Vergleichs Rückgabewert gibt.
  * @param {*} a Ein Objekt
  * @param {*} b Ein anderes Objekt
  * @param {*} sortingCriteria das Sortierkriterium als String, Möglichkeiten: 'betten','schutzkleidung','personal','materialien'
  * @param {*} path Pfad Möglichkeiten: 'ressourcen','adresse'
  */
 function _sumCompare(a, b, sortingCriteria,path){
    //Extrahiere die gewünschten Attribut Objekte
    let aAttributes = a[path][sortingCriteria]
    let bAttributes = b[path][sortingCriteria]
    console.log('_sumcompare',aAttributes,bAttributes)

    //Extrahiere Attributs Werte
    let aValues = Object.values(aAttributes)
    let bValues = Object.values(bAttributes)

    //Fitriere und addiere zählbare Integer Werte, keine Strings oder Auslastungszahlen!
    let aCompare = 0;
    let bCompare = 0;

    aValues.forEach(element => {
      if(element%1 == 0 && (typeof element) == 'number'){
        aCompare = aCompare + element
      }
    });

    bValues.forEach(element => {
      if(element%1 == 0 && (typeof element) == 'number'){
        bCompare = bCompare + element
      }
    });
    console.log('_sum',aCompare,bCompare)
    //Vergleiche nun Beide Objekte anhand des Summenwertes
    return _absoluteCompare(aCompare,bCompare)
 }


 /**
  * Komplexere Hilfsfunktion für Array.sort() speziell für Krankenhaus Objekte, die jeweils die Auslastung der Betten miteinander gewichtet vergleicht.
  * Gibt den Vergleichs Rückgabewert zurück.
  * @param {*} a Ein Objekt
  * @param {*} b Ein anderes Objekt
  */
 function _bedsCompare(a,b){
   //Anzahl Intensivbetten aus Objekten holen.
   let aIntensivbetten = a['ressourcen']['betten']['intensiv']
   let bIntensivbetten = b['ressourcen']['betten']['intensiv']

   //Anzahl Normalbetten aus Objekten holen.
   let aNormalbetten = a['ressourcen']['betten']['normal']
   let bNormalbetten = b['ressourcen']['betten']['normal']

   //Intensivauslastung aus Objekten holen.
   let aIntensivauslastung = a['ressourcen']['betten']['intensivauslastung']
   let bIntensivauslastung = b['ressourcen']['betten']['intensivauslastung']

   //Normalauslastung aus Objekten holen.
   let aNormalauslastung = a['ressourcen']['betten']['normalauslastung']
   let bNormalauslastung = b['ressourcen']['betten']['normalauslastung']

   //Gewichtete Gesamtauslastungen berechnen für a und b Krankenhausobjekte.
   let aAuslastung = (aIntensivauslastung * aIntensivbetten + aNormalauslastung * aNormalbetten)/(aIntensivbetten + aNormalbetten);
   let bAuslastung = (bIntensivauslastung * bIntensivbetten + bNormalauslastung * bNormalbetten)/(bIntensivbetten + bNormalbetten);

   //Gewichtete Auslastungen vergleichen und Wert zurück geben.
   return _absoluteCompare(aAuslastung,bAuslastung)

 }


 /**
  * Hilfsfunktion für Hilfsfunktionen, vermindert Coderedundanz.
  * Hier passiert das eigentliche Verglecihen von zwei Werten.
  * @param {*} aValue number
  * @param {*} bValue number
  */
 function _absoluteCompare(aValue, bValue){
  if ( aValue < bValue ){
    return -1;
  }
  if ( aValue > bValue ){
    return 1;
  }
  return 0;
 }


 //Ende Sortierfunktionen
 //Anfang Filterfunktionen


 /**
  * Filtert eine Liste von Krankenhausobjekten nach gegebenem Kriterium
  * @param {*} list Liste von Krankenhausobjekten
  * @param {*} filterCriteria 'eriador','rhovanion','rohan','mordor','nord','süd','ost','west','krankenhaus','klinik','andere','default'
  */
 function _filterList(list,filterCriteria){
   //Filtrierte Liste
   let filteredList = []

   switch(filterCriteria){
      //Filtern nach Regionen
      case 'eriador':
        filteredList = _filterByCoordinates(list,-19,-10,3,12)
        break;
      case 'rhovanion':
        filteredList = _filterByCoordinates(list,-7,0,4,11)
        break;
      case 'rohan':
        filteredList = _filterByCoordinates(list,-8,-4,1,4)
        break;
      case 'mordor':
        filteredList = _filterByCoordinates(list,-2,7,-4,2)
        break;

      //Filtern nach Himmelsrichtungsgebieten
      case 'nord':
        filteredList = _filterByCoordinates(list,-19,7,3,15)
        break;
      case 'süd':
        filteredList = _filterByCoordinates(list,-19,7,-9,3)
        break;
      case 'ost':
        filteredList = _filterByCoordinates(list,-6,7,-9,15)
        break;
      case 'west':
        filteredList = _filterByCoordinates(list,-19,-6,-9,15)
        break;

      //Filtern nach Krankenhaustypen
      case 'krankenhaus':
      case 'klinik':
        filteredList = _filterByName(list,filterCriteria,true)
        break;
      case 'andere':
        filteredList = _filterByName(list,'krankenhaus',false)
        filteredList = _filterByName(filteredList,'klinik',false)
        break;

      //bei default nix ändern
      default:
        filteredList = list
        break;
   }
   //return
   return filteredList
 }


 /**
  * Filtert Liste von Krankenhausobjekten nach Koordinaten.
  * @param {*} list Liste von Krankenhausobjekten
  * @param {*} minLat 
  * @param {*} maxLat 
  * @param {*} minLon 
  * @param {*} maxLon 
  */
 function _filterByCoordinates(list,minLat,maxLat,minLon,maxLon){
    //Filtrierte Liste
    let filteredList = []
    list.forEach(element => {
      //Extrahiere Koordinaten
      let lat = element['koordinaten']['lat']
      let lon = element['koordinaten']['lon']

      //Vergleiche mit Grenzwerten
      if(minLat <= lat && lat <= maxLat && minLon <= lon && lon <= maxLon){
        //Wenn es im Kasten ist, in Filtrierte Liste pushen
        filteredList.push(element)
      }
    });
    return filteredList
 }

 
 /**
  * Filtriert Liste von Krankenhausobjekten nach Namen, die einen gewissen Substring enthalten oder nicht.
  * @param {*} list Liste von Krankenhausobjekten
  * @param {*} substring Substring
  * @param {*} containsSubstring true oder false
  */
 function _filterByName(list,substring,containsSubstring){
  //Filtrierte Liste
  let filteredList = []

  list.forEach(element => {
    //extrahiere element name
    let name = element['name'] 
    name = name.toLowerCase();

    if(name.includes(substring) == containsSubstring){
      //Wenn Element substring enthält oder nicht, pushen
      filteredList.push(element)
    }
  });
  return filteredList
 }

